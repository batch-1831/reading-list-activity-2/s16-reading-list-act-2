// -----------1st Activity-----------

let inputName = prompt("Enter your name: ");
printName(inputName);

function printName(name) {
    if(name.length > 7) {

        for(count = name.length - 1; count >= 0; count--) {
            if (name[count].toLowerCase() == 'a' || name[count].toLowerCase() == 'e' || name[count].toLowerCase() == 'i' || name[count].toLowerCase() == 'o' || name[count].toLowerCase() == 'u') {
                continue;
            }
            else {
                console.log(name[count])
            }
        }
    }
    else {
        // convert string to array
        inputName = inputName.split('');
        // reverse array order
        reverseName = inputName.reverse();
        // using .forEach
        reverseName.forEach(reverseName => { console.log(reverseName) });

        // using for loop (other method)
        // for (i = 0; i < reverseName.length; i++) {
        //     console.log(reverseName[i]);
        // }
    }
}


// -----------2nd Activiity-----------
// Declare array
let students = ['John', 'Jane', 'Joe'];

// 1. Total Array Size
console.log("1. Total number of students: " + students.length);

// 2. Add Jack at the beginning of array
students.unshift('Jack');
console.log("2. Students: [" + students.join(", ") + "]");

// 3. Add Jill at the end of array
students.push('Jill');
console.log("3. Students: [" + students.join(", ") + "]");

// 4. Remove an element at the beginning
students.shift();
console.log("4. Students: [" + students.join(", ") + "]");

// 5. Remove an element at the end
students.pop();
console.log("5. Students: [" + students.join(", ") + "]");

// 6. Index number at the end of the array
let indexLast = students.length - 1;
console.log("6. Last index of the current array: " + indexLast);


// -----------3rd Activity-----------

console.log("Initial content of array: [" + students + "]");

findRemove(students);

function findRemove(studentNames) {
    let studentIndex = studentNames.indexOf('Jane');
    students.splice(studentIndex, 1);
    console.log('After finding the index and removing the student "Jane": [' + students.join(", ") + ']');
}


// -----------4th Activity-----------

// define object
let person = {
    firstName: 'Juan',
    lastName: 'Dela Cruz',
    nickName:  'J',
    age: '25',
    address: {
        city: 'Quezon City',
        country: 'Philippines'
    },
    friends: {
        friend1: 'John',
        friend2: 'Jane',
        friend3: 'Joe'
    },
    fn: function() {
        console.log("Using object: ")

        console.log("My Name is " + this.firstName + " " + this.lastName + ", but you can call me " + this.nickName + ". I am " + this.age + " years old and I live in " + this.address.city + ", " + this.address.country + ". My friends are " + this.friends.friend1 + ", " + this.friends.friend2 + " and " + this.friends.friend3 + ".");
    }
}

// Invoke function
person.fn();


// -----------5th Activity-----------

// copy object by using deep clone method
let personClone = JSON.parse(JSON.stringify(person));

// desctructuring object
const {
        firstName: fName, 
        lastName: lName, 
        nickName: nName, 
        age: ageCopy, 
        address: addressCopy, 
        friends: friendsCopy
} = personClone;

// using template literals
console.log("Using literals: ")

console.log(`My name is ${fName} ${lName}, but you can call me ${nName}. I am ${ageCopy} years old and I live in ${addressCopy.city}, ${addressCopy.country}. My friends are ${friendsCopy.friend1}, and ${friendsCopy.friend2}, and ${friendsCopy.friend3}.`);